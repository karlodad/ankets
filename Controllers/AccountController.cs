﻿using System;
using System.Collections.Generic;
using System.Security.Claims;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Authentication.Cookies;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using WebApplication1.Models;
using WebApplication1.Service;
using WebApplication1.ViewModels;

namespace WebApplication1.Controllers
{
    public class AccountController : Controller
    {
        private readonly UserService userService;
        private readonly ResultService resultService;
        public AccountController(UserService userService, ResultService resultService)
        {
            this.userService = userService;
            this.resultService = resultService;
        }

        [HttpGet]
        public IActionResult Login()
        {
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult Login(LoginModel model)
        {
            if (ModelState.IsValid)
            {
                User user = userService.CheckUser(new Models.User() { Login = model.Login, Password = model.Password });
                if (user != null)
                {
                    Authenticate(model.Login, model.Admin);
                    return RedirectToAction("Index", "Home");
                }
                ModelState.AddModelError("", "Некорректные логин и(или) пароль");
            }
            return View(model);
        }

        [HttpGet]
        public IActionResult Register()
        {
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult Register(RegisterModel model)
        {
            if (ModelState.IsValid)
            {
                userService.Register(new User() { Login = model.Login, Password = model.Password });
                Authenticate(model.Login, false);
                return RedirectToAction("Index", "Home");
            }
            return View(model);
        }

        [AcceptVerbs("Get", "Post")]
        public IActionResult CheckLogin(string login)
        {
            if (userService.CheckLogin(new User() { Login = login }) != null)
                return Json(false);
            return Json(true);
        }

        private void Authenticate(string userName, Boolean admin)
        {
            var claims = new List<Claim>
            {
                new Claim(ClaimsIdentity.DefaultNameClaimType, userName),
                new Claim(ClaimsIdentity.DefaultRoleClaimType, admin ? "admin" : "user")
            };
            ClaimsIdentity id = new ClaimsIdentity(claims, "AppCookie", ClaimsIdentity.DefaultNameClaimType, ClaimsIdentity.DefaultRoleClaimType);
            HttpContext.SignInAsync(CookieAuthenticationDefaults.AuthenticationScheme, new ClaimsPrincipal(id));
        }

        [Authorize]
        public IActionResult Logout()
        {
            HttpContext.SignOutAsync(CookieAuthenticationDefaults.AuthenticationScheme);
            return RedirectToAction("Login", "Account");
        }

        [Authorize]
        public IActionResult Room()
        {
            return View(resultService.FindByLogin(User.Identity.Name));
        }

        [Authorize]
        public IActionResult Test(long id)
        {
            Result result = resultService.FindById(id);
            if(!result.User.Login.Equals(User.Identity.Name)) return RedirectToAction("Room", "Account");

            return View(result);
        }

    }
}