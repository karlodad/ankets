﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using WebApplication1.Models;
using WebApplication1.Service;

namespace WebApplication1.Controllers
{
    [Authorize(Roles = "admin")]
    public class AnketController : Controller
    {
        private readonly AnketService anketService;
        public AnketController(AnketService anketService)
        {
            this.anketService = anketService;
        }

        [HttpGet]
        public IActionResult Add()
        {
            return View();
        }

        [HttpGet]
        public IActionResult Edit(long id)
        {
            return View(anketService.FindById(id));
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult Edit(Anket anket)
        {
            if (ModelState.IsValid)
            {
                anketService.Save(anket);
                return RedirectToAction("Index", "Home");
            }
            return View(anket);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult Add(Anket anket)
        {
            if (ModelState.IsValid)
            {
                anketService.Save(anket);
                return RedirectToAction("Index", "Home");                
            }
            return View(anket);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult Remove(long id)
        {
            anketService.Remove(id);
            return RedirectToAction("Index", "Home");
        }


        [AcceptVerbs("Get", "Post")]
        public IActionResult CheckName(string name)
        {
            if (anketService.CheckName(name))
                return Json(true);
            return Json(false);
        }
    }
}