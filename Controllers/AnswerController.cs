﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using WebApplication1.Models;
using WebApplication1.Service;

namespace WebApplication1.Controllers
{
    [Authorize(Roles = "admin")]
    public class AnswerController : Controller
    {
        private readonly QuestionService questionService;
        private readonly AnswerService answerService;

        public AnswerController(QuestionService questionService, AnswerService answerService)
        {
            this.questionService = questionService;
            this.answerService = answerService;
        }

        public IActionResult Index(long id)
        {
            ViewBag.Question = questionService.FindById(id);
            return View(answerService.FindByQuestionId(id));
        }

        [HttpGet]
        public IActionResult Add(long id)
        {
            if(answerService.CountAnswersByQuestionId(id)<=3)
                return View(new Answer() { Question_id = id, Question = questionService.FindById(id) });
            return RedirectToAction("Index","Anwer", new { id });
        }

        [HttpGet]
        public IActionResult Edit(long id)
        {
            return View(answerService.FindById(id));
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult Edit(Answer answer)
        {
            if (ModelState.IsValid)
            {
                answerService.Save(answer);
                return RedirectToAction("Index", "Answer", new { id = answer.Question_id });
            }
            answer.Question = questionService.FindById(answer.Question_id);
            return View(answer);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult Add(Answer answer)
        {
            if (ModelState.IsValid)
            {
                answerService.Save(answer);
                return RedirectToAction("Index", "Answer", new { id = answer.Question_id });
            }
            return View(answer);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult Remove(long id)
        {
            Answer answer = answerService.FindById(id);
            answerService.Remove(id);
            return RedirectToAction("Index", "Answer", new { id = answer.Question_id });
        }


        [AcceptVerbs("Get", "Post")]
        public IActionResult CheckTrueAnswer(bool checks, long question_id)
        {
            if (!checks || (checks && answerService.CountTrueAnswersByQuestionId(question_id) == 0)) 
                return Json(true);
            return Json(false);
        }

        [AcceptVerbs("Get", "Post")]
        public IActionResult CheckUniqueText(string text, long question_id)
        {
            if (answerService.CountUniqueText(text, question_id) == 0)
                return Json(true);
            return Json(false);
        }
        
    }
}