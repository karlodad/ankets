﻿using System.Diagnostics;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using WebApplication1.Models;
using WebApplication1.Service;

namespace WebApplication1.Controllers
{
    public class HomeController : Controller
    {
        private readonly AnketService anketService;

        public HomeController(AnketService anketService)
        {
            this.anketService = anketService;
        }

        [Authorize]
        public IActionResult Index()
        {
            /*if (User.Identity.IsAuthenticated)
            {
                ViewBag.Message = User.Identity.Name;
                ViewBag.Role = User.FindFirst(x => x.Type == ClaimsIdentity.DefaultRoleClaimType).Value;
            }*/
            return View(anketService.FindAll());
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}
