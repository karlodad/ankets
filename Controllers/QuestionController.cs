﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using WebApplication1.Models;
using WebApplication1.Service;

namespace WebApplication1.Controllers
{
    [Authorize(Roles = "admin")]
    public class QuestionController : Controller
    {
        private readonly QuestionService questionService;
        private readonly AnketService anketService;

        public QuestionController(QuestionService questionService, AnketService anketService)
        {
            this.questionService = questionService;
            this.anketService = anketService;
        }

        public IActionResult Index(long id)
        {
            ViewBag.Anket = anketService.FindById(id);
            return View(questionService.FindByAnketId(id));
        }

        [HttpGet]
        public IActionResult Add(long id)
        {
            return View(new Question() { Anket_id = id });
        }

        [HttpGet]
        public IActionResult Edit(long id)
        {
            return View(questionService.FindById(id));
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult Edit(Question question)
        {
            if (ModelState.IsValid)
            {
                questionService.Save(question);
                return RedirectToAction("Index", "Question", new { id = question.Anket_id });
            }
            return View(question);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult Add(Question question)
        {
            if (ModelState.IsValid)
            {
                questionService.Save(question);
                return RedirectToAction("Index", "Question", new { id = question.Anket_id });
            }
            return View(question);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult Remove(long id)
        {
            Question question = questionService.FindById(id);
            questionService.Remove(id);
            return RedirectToAction("Index", "Question", new { id = question.Anket_id });
        }


        [AcceptVerbs("Get", "Post")]
        public IActionResult CheckName(string name)
        {
            if (questionService.CheckName(name))
                return Json(true);
            return Json(false);
        }
    }
}