﻿using Microsoft.AspNetCore.Mvc;
using WebApplication1.Service;

namespace WebApplication1.Controllers
{
    public class StatisticController : Controller
    {
        private readonly ResultService resultService;

        public StatisticController(ResultService resultService)
        {
            this.resultService = resultService;
        }

        public IActionResult Index()
        {
            return View(resultService.FindAllStatistic());
        }
    }
}