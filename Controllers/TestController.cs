﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using WebApplication1.Models;
using WebApplication1.Service;

namespace WebApplication1.Controllers
{
    [Authorize]
    public class TestController : Controller
    {
        private readonly AnketService anketService;
        private readonly ResultService resultService;

        public TestController(AnketService anketService, ResultService resultService)
        {
            this.anketService = anketService;
            this.resultService = resultService;
        }

        public IActionResult Run(long id)
        {
            ViewBag.DateNow = DateTime.Now;
            Anket anket = anketService.FindById(id);
            anket.Questions = anket.Questions
                .OrderBy(a => Guid.NewGuid()).ToList()
                .GetRange(0, anket.Questions.Count / 2);
            return View(anket);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult Run(Dictionary<String, String> values)
        {
            DateTime date_begin = DateTime.Parse(values["DateNow"]);
            int anket_id = Convert.ToInt32(values["anket_id"]);
            Result result = resultService.Save(values, User.Identity.Name, anket_id, date_begin, DateTime.Now);
            return View("Answer", result);
        }
    }
}