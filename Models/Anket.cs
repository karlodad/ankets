﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace WebApplication1.Models
{
    public class Anket : BaseEntity
    {
        public Anket() { }

        [Required(ErrorMessage = "Не указано название анкеты")]
        [StringLength(20, MinimumLength = 1, ErrorMessage = "Длина строки должна быть от 1 до 20 символов")]
        [Remote(action: "CheckName", controller: "Anket", ErrorMessage = "Такое название уже есть")]
        public String Name { get; set; }
        public List<Question> Questions { get; set; }

    }
}
