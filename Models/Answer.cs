﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace WebApplication1.Models
{
    public class Answer : BaseEntity
    {
        public Answer() { }

        [Required(ErrorMessage = "Не указан ответ")]
        [StringLength(50, MinimumLength = 1, ErrorMessage = "Длина строки должна быть от 1 до 50 символов")]
        [Remote(action: "CheckUniqueText", controller: "Answer", AdditionalFields = nameof(Question_id), ErrorMessage = "Такой ответ на данный вопрос уже есть")]
        public String Text { get; set; }

        [Display(Name ="Правильный")]
        [Remote(action: "CheckTrueAnswer", controller: "Answer", AdditionalFields = nameof(Question_id), ErrorMessage = "В одном вопросе может быть только 1 правильный ответ")]
        public Boolean Checks { get; set; }
        public long Question_id { get; set; }
        public Question Question { get; set; }

    }
    public static class Extension
    {
        public static string ToCheckString(this Boolean check)
        {
            if (check) return "Верный";
            return "Не верный";
        }
    }
}
