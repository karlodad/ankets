﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace WebApplication1.Models
{
    public class Question : BaseEntity
    {
        public Question() { }

        [Required(ErrorMessage = "Не указано название вопроса")]
        [StringLength(100, MinimumLength = 1, ErrorMessage = "Длина строки должна быть от 1 до 100 символов")]
        [Remote(action: "CheckName", controller: "Question", ErrorMessage = "Такое название уже есть")]
        public String Name { get; set; }
        public long Anket_id { get; set; }
        public Anket Anket { get; set; }
        public List<Answer> Answers { get; set; }

    }
}

