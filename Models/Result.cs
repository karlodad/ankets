﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace WebApplication1.Models
{
    public class Result : BaseEntity
    {
        public Result() { }

        [Display(Name = "Результат")]
        public Int32 Score { get; set; }

        [Display(Name = "Дата начала")]
        public DateTime Date_begin { get; set; }

        [Display(Name = "Дата конца")]
        public DateTime Date_end { get; set; }

        [Display(Name = "Длительность")]
        public int Second { get; set; }

        public long User_id { get; set; }
        public User User { get; set; }


        public long Anket_id { get; set; }
        public Anket Anket { get; set; }

        public int[] Answers { get; set; }
        public int[] Questions { get; set; }

    }
}
