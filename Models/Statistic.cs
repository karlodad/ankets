﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebApplication1.Models
{
    public class Statistic : BaseEntity
    {
        public Statistic() { }

        public String Header { get; set; }

        public List<String> Body { get; set; }
    }
}
