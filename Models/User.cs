﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace WebApplication1.Models
{
    public class User : BaseEntity
    {
        public User() { }
        [Display(Name = "Пользователь")]
        public string Login { get; set; }
        public string Password { get; set; }
    }
}
