﻿using System;
using System.Collections.Generic;
using Microsoft.Extensions.Configuration;
using WebApplication1.Models;
using WebApplication1.Repository.Dao;

namespace WebApplication1.Repository
{
    public class AnketRepository : Repository, IAnketDao
    {
        public AnketRepository(IConfiguration configuration) : base(configuration) { }

        public void create(Anket obj)
        {
            Execute(false, "INSERT INTO ankets(name) VALUES(@1)", obj.Name);
        }

        public void delete(long id)
        {
            Execute(false, "DELETE FROM ankets WHERE id = @1", id);
        }

        public List<Anket> FindAll()
        {
            List<Dictionary<String, Object>> fields = Execute(true, "SELECT id, name FROM ankets ORDER BY name ASC");

            List<Anket> ankets = new List<Anket>();
            foreach (Dictionary<String, Object> item in fields)
            {
                ankets.Add(new Anket
                {
                    Id = Convert.ToInt32(item["id"]),
                    Name = item["name"].ToString()
                });
            }
            return ankets;            
        }

        public Anket FindByName(string name)
        {
            List<Dictionary<String, Object>> fields;
            fields = Execute(true, "SELECT id, name FROM ankets WHERE name = @1", name);
            if (fields.Count != 1) return null;
            return new Anket
            {
                Id = Convert.ToInt32(fields[0]["id"]),
                Name = fields[0]["name"].ToString()
            };
        }

        public Anket read(long id)
        {
            List<Dictionary<String, Object>> fields;
            fields = Execute(true, "SELECT name FROM ankets WHERE id = @1", id);
            if (fields.Count != 1) return null;
            return new Anket
            {
                Id = id,
                Name = fields[0]["name"].ToString()
            };
        }

        public void update(Anket obj)
        {
            Execute(false, "UPDATE ankets SET name = @2 WHERE id = @1", obj.Id, obj.Name);
        }
    }
}
