﻿using System;
using System.Collections.Generic;
using Microsoft.Extensions.Configuration;
using WebApplication1.Models;
using WebApplication1.Repository.Dao;

namespace WebApplication1.Repository
{
    public class AnswerRepository : Repository, IAnswerDao
    {
        public AnswerRepository(IConfiguration configuration) : base(configuration) { }

        public int CountAnswersByQuestionId(long id)
        {
            List<Dictionary<String, Object>> fields;
            fields = Execute(true, "SELECT count(*) as count FROM answers WHERE question_id = @1", id);
            return Convert.ToInt32(fields[0]["count"].ToString());
        }

        public int CountAnswerWithTextAndQuestionId(string text, long id)
        {
            List<Dictionary<String, Object>> fields;
            fields = Execute(true, "select count(*) as count FROM answers WHERE text = @1 AND question_id = @2", text, id);
            return Convert.ToInt32(fields[0]["count"].ToString());
        }

        public int CountTrueAnswersByQuestionId(long id)
        {
            List<Dictionary<String, Object>> fields;
            fields = Execute(true, "select count(*) as count FROM answers WHERE checks = true AND question_id = @1", id);
            return Convert.ToInt32(fields[0]["count"].ToString());
        }

        public void create(Answer obj)
        {
            Execute(false, "INSERT INTO answers(text, checks, question_id) VALUES(@1, @2, @3)", obj.Text, obj.Checks, obj.Question_id);
        }

        public void delete(long id)
        {
            Execute(false, "DELETE FROM answers WHERE id = @1", id);
        }

        public List<Answer> FindByQuestion(Question question)
        {
            List<Dictionary<String, Object>> fields = Execute(true, "SELECT id, text, checks FROM answers WHERE question_id = @1 ORDER BY text ASC", question.Id);
            List<Answer> answers = new List<Answer>();
            foreach (Dictionary<String, Object> item in fields)
            {
                answers.Add(new Answer
                {
                    Id = Convert.ToInt32(item["id"].ToString()),
                    Text = item["text"].ToString(),
                    Checks = Convert.ToBoolean(item["checks"]),
                    Question_id = question.Id,
                    Question = question
                });
            }
            return answers;
        }

        public Answer read(long id)
        {
            List<Dictionary<String, Object>> fields;
            fields = Execute(true, "SELECT text, checks, question_id FROM answers WHERE id = @1", id);
            return new Answer
            {
                Id = id,
                Text = fields[0]["text"].ToString(),
                Checks = Convert.ToBoolean(fields[0]["checks"]),
                Question_id = Convert.ToInt32(fields[0]["question_id"].ToString())
            };
        }

        public void update(Answer obj)
        {
            Execute(false, "UPDATE answers SET text = @2, checks = @3, question_id = @4 WHERE id = @1", obj.Id, obj.Text, obj.Checks, obj.Question_id);
        }
    }
}
