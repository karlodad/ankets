﻿using System;
using System.Collections.Generic;
using WebApplication1.Models;

namespace WebApplication1.Repository.Dao
{
    public interface IAnketDao : IGenericDao<long, Anket>
    {
        Anket FindByName(String name);
        List<Anket> FindAll();
    }
}
