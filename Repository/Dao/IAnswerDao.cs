﻿using System.Collections.Generic;
using WebApplication1.Models;

namespace WebApplication1.Repository.Dao
{
    public interface IAnswerDao : IGenericDao<long, Answer>
    {
        List<Answer> FindByQuestion(Question question);
        int CountAnswersByQuestionId(long id);
        int CountTrueAnswersByQuestionId(long id);
        int CountAnswerWithTextAndQuestionId(string text, long id);
    }
}
