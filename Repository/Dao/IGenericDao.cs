﻿
using WebApplication1.Models;

namespace WebApplication1.Repository.Dao
{
    public interface IGenericDao<ID, T> : IDao<T> where T : BaseEntity
    {
        void create(T obj);
        T read(ID id);
        void update(T obj);
        void delete(ID id);
    }
}
