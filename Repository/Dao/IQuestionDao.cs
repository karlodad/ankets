﻿using System;
using System.Collections.Generic;
using WebApplication1.Models;

namespace WebApplication1.Repository.Dao
{
    public interface IQuestionDao : IGenericDao<long, Question>
    {
        List<Question> FindByAnket(Anket anket);
        Question FindByName(String name);
        List<Question> FindByAnketAndQuestionsId(Anket anket, int[] questionIds);
    }
}
