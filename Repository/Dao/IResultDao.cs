﻿using System.Collections.Generic;
using WebApplication1.Models;

namespace WebApplication1.Repository.Dao
{
    public interface IResultDao : IGenericDao<long, Result>
    {
        List<Result> FindByUser(User user);

        Statistic GetMinAvgScore();
        Statistic GetMaxAvgScore();
        Statistic GetMaxSecondTest();
        Statistic GetMostPopularTest();
    }
}
