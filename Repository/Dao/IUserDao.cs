﻿using System;
using WebApplication1.Models;

namespace WebApplication1.Repository.Dao
{
    public interface IUserDao : IGenericDao<long, User>
    {
        User FindByLogin(String login);
        User FindByUser(User user);
    }
}
