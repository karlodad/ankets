﻿using System;
using System.Collections.Generic;
using Microsoft.Extensions.Configuration;
using WebApplication1.Models;
using WebApplication1.Repository.Dao;

namespace WebApplication1.Repository
{
    public class QuestionRepository : Repository, IQuestionDao
    {
        public QuestionRepository(IConfiguration configuration) : base(configuration) { }

        public void create(Question obj)
        {
            Execute(false, "INSERT INTO questions(name, anket_id) VALUES(@1, @2)", obj.Name, obj.Anket_id);
        }

        public void delete(long id)
        {
            Execute(false, "DELETE FROM questions WHERE id = @1", id);
        }

        public List<Question> FindByAnket(Anket anket)
        {
            List<Dictionary<String, Object>> fields = Execute(true, "SELECT id, name FROM questions WHERE anket_id = @1 ORDER BY name ASC", anket.Id);
            List<Question> questions = new List<Question>();
            foreach (Dictionary<String, Object> item in fields)
            {
                questions.Add(new Question
                {
                    Id = Convert.ToInt32(item["id"].ToString()),
                    Name = item["name"].ToString(),
                    Anket_id = anket.Id,
                    Anket = anket
                });
            }
            return questions;
        }

        public List<Question> FindByAnketAndQuestionsId(Anket anket, int[] questionIds)
        {
            List<Dictionary<String, Object>> fields;
            fields = Execute(true, "SELECT id, name FROM questions WHERE anket_id = @1 AND id = ANY (@2) ORDER BY name ASC", anket.Id, questionIds);
            List<Question> questions = new List<Question>();
            foreach (Dictionary<String, Object> item in fields)
            {
                questions.Add(new Question
                {
                    Id = Convert.ToInt32(item["id"].ToString()),
                    Name = item["name"].ToString(),
                    Anket_id = anket.Id,
                    Anket = anket
                });
            }
            return questions;
        }

        public Question FindByName(string name)
        {
            List<Dictionary<String, Object>> fields;
            fields = Execute(true, "SELECT id, name, anket_id FROM questions WHERE name = @1", name);
            if (fields.Count != 1) return null;
            return new Question
            {
                Id = Convert.ToInt32(fields[0]["id"]),
                Name = fields[0]["name"].ToString(),
                Anket_id = Convert.ToInt32(fields[0]["anket_id"].ToString())
            };
        }

        public Question read(long id)
        {
            List<Dictionary<String, Object>> fields;
            fields = Execute(true, "SELECT name, anket_id FROM questions WHERE id = @1", id);
            if (fields.Count != 1) return null;
            return new Question
            {
                Id = id,
                Name = fields[0]["name"].ToString(),
                Anket_id = Convert.ToInt32(fields[0]["anket_id"].ToString())
            };
        }

        public void update(Question obj)
        {
            Execute(false, "UPDATE questions SET name = @2, anket_id = @3 WHERE id = @1", obj.Id, obj.Name, obj.Anket_id);
        }
    }
}
