﻿using Microsoft.Extensions.Configuration;
using Npgsql;
using System;
using System.Collections.Generic;
using System.Data;

namespace WebApplication1.Repository
{
    public abstract class Repository
    {
        private readonly IConfiguration configuration;

        public Repository(IConfiguration configuration)
        {
            this.configuration = configuration;
        }

        private IDbConnection Connection
        {
            get
            {
                return new NpgsqlConnection(configuration.GetValue<string>("DBInfo:ConnectionString"));
            }
        }
        protected List<Dictionary<String, Object>> Execute(Boolean returned, String sql, params Object[] values)
        {
            using IDbConnection dbConnection = Connection;
            var command = dbConnection.CreateCommand();
            command.CommandText = sql;
            AddParameters(command, values);
            dbConnection.Open();
            if (!returned)
            {
                command.ExecuteNonQuery();
            }
            else
            {
                List<Dictionary<String, Object>> answers = new List<Dictionary<String, Object>>();
                using (var result = command.ExecuteReader())
                {
                    while (result.Read())
                    {
                        Dictionary<String, Object> answer = new Dictionary<String, Object>();
                        for (int i = 0; i < result.FieldCount; i++)
                        {
                            answer[result.GetName(i)] = result[result.GetName(i)];
                        }
                        answers.Add(answer);
                    }
                }
                return answers;
            }
            return null;
        }

        private void AddParameters(IDbCommand idb, Object[] values)
        {
            if (values == null) return;
            for(int i = 0; i < values.Length; i++)
            {
                var param = idb.CreateParameter();
                param.ParameterName = "@" + (i + 1);
                param.Value = values[i];
                idb.Parameters.Add(param);
            }
        }
    }
}
