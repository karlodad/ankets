﻿using System;
using System.Collections.Generic;
using Microsoft.Extensions.Configuration;
using Newtonsoft.Json;
using WebApplication1.Models;
using WebApplication1.Repository.Dao;

namespace WebApplication1.Repository
{
    public class ResultRepository : Repository, IResultDao
    {
        public ResultRepository(IConfiguration configuration) : base(configuration) { }

        public void create(Result obj)
        {
            Execute(false, "INSERT INTO results(score, date_begin, date_end, user_id, anket_id, ans, quest, second) " +
                "VALUES(@1, @2, @3, @4, @5, @6, @7, @8)",
                obj.Score, obj.Date_begin, obj.Date_end,
                obj.User_id, obj.Anket_id, obj.Answers, obj.Questions, obj.Second);
        }

        public void delete(long id)
        {
            Execute(false, "DELETE FROM results WHERE id = @1", id);
        }

        public List<Result> FindByUser(User user)
        {
            List<Dictionary<String, Object>> fields;
            fields = Execute(true, "SELECT id, score, date_begin, date_end, user_id, anket_id, ans, quest, second " +
                "FROM results WHERE user_id = @1 ORDER BY date_begin DESC", user.Id);
            List<Result> results = new List<Result>();
            foreach (Dictionary<String, Object> item in fields)
            {
                results.Add(new Result
                {
                    Id = Convert.ToInt32(item["id"].ToString()),
                    Score = Convert.ToInt32(item["score"]),
                    Date_begin = Convert.ToDateTime(item["date_begin"]),
                    Date_end = Convert.ToDateTime(item["date_end"]),
                    User_id = Convert.ToInt32(item["user_id"]),
                    User = user,
                    Anket_id = Convert.ToInt32(item["anket_id"]),
                    Answers = (int[])(item["ans"]),
                    Questions = (int[])(item["quest"]),
                    Second = Convert.ToInt32(item["second"])
                });
            }
            return results;
        }

        public Statistic GetMaxAvgScore()
        {
            List<Dictionary<String, Object>> fields;
            fields = Execute(true, "SELECT users.login, avg(score) as count " +
                "from results " +
                "join users on users.id = results.user_id " +
                "group by users.login " +
                "order by count DESC " +
                "LIMIT 1", null);
            if (fields.Count != 1) return null;
            return new Statistic
            {
                Header = "Максимальный средний результат",
                Body = new List<String>() { "Результат: " + Double.Parse(fields[0]["count"].ToString()), "Пользователь: " + fields[0]["login"].ToString() }
            };
        }

        public Statistic GetMaxSecondTest()
        {
            List<Dictionary<String, Object>> fields;
            fields = Execute(true, "SELECT users.login, ankets.name, max(second) as count " +
                "from results " +
                "join users on users.id = results.user_id " +
                "join ankets on ankets.id = results.anket_id " +
                "group by users.login, ankets.name " +
                "order by count DESC " +
                "LIMIT 1", null);
            if (fields.Count != 1) return null;
            return new Statistic
            {
                Header = "Самое долгое прохождение теста",
                Body = new List<String>() { "Секунд: " + fields[0]["count"].ToString(), "Название анкеты: " + fields[0]["name"].ToString(), "Пользователь: " + fields[0]["login"].ToString() }
            };
        }

        public Statistic GetMinAvgScore()
        {
            List<Dictionary<String, Object>> fields;
            fields = Execute(true, "SELECT users.login, avg(score) as count " +
                "from results " +
                "join users on users.id = results.user_id " +
                "group by users.login " +
                "order by count ASC " +
                "LIMIT 1", null);
            if (fields.Count != 1) return null;
            return new Statistic
            {
                Header = "Минимальный средний результат",
                Body = new List<String>() { "Результат: " + Double.Parse(fields[0]["count"].ToString()), "Пользователь: " + fields[0]["login"].ToString() }
            };
        }

        public Statistic GetMostPopularTest()
        {
            List<Dictionary<String, Object>> fields;
            fields = Execute(true, "SELECT ankets.name, count(*) as count " +
                "from results join ankets on ankets.id = results.anket_id " +
                "group by ankets.name " +
                "order by count DESC " +
                "LIMIT 1", null);
            if (fields.Count != 1) return null;
            return new Statistic
            {
                Header = "Самый популярный тест",
                Body = new List<String>() { "Количество попыток: " + fields[0]["count"].ToString(), "Название анкеты: " + fields[0]["name"].ToString() }
            };
        }

        public Result read(long id)
        {
            List<Dictionary<String, Object>> fields;
            fields = Execute(true, "SELECT score, date_begin, date_end, user_id, anket_id, ans, quest, second FROM results WHERE id = @1", id);
            if (fields.Count != 1) return null;
            return new Result
            {
                Id = id,
                Score = Convert.ToInt32(fields[0]["score"]),
                Date_begin = Convert.ToDateTime(fields[0]["date_begin"]),
                Date_end = Convert.ToDateTime(fields[0]["date_end"]),
                User_id = Convert.ToInt32(fields[0]["user_id"]),
                Anket_id = Convert.ToInt32(fields[0]["anket_id"]),
                Answers = (int[])(fields[0]["ans"]),
                Questions = (int[])(fields[0]["quest"]),
                Second = Convert.ToInt32(fields[0]["second"])
            };
        }

        public void update(Result obj)
        {
            Execute(false, "UPDATE results " +
                "SET score = @2, date_begin = @3, date_end = @4, user_id = @5, anket_id = @6, ans = @7,ans = @8, second = @9 " +
                "WHERE id = @1", obj.Id, obj.Score, obj.Date_begin, obj.Date_end, obj.User_id, obj.Anket_id, obj.Answers, obj.Questions, obj.Second);
        }
    }
}
