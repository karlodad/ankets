﻿using System;
using System.Collections.Generic;
using Microsoft.Extensions.Configuration;
using WebApplication1.Models;
using WebApplication1.Repository.Dao;

namespace WebApplication1.Repository
{
    public class UserRepository : Repository, IUserDao
    {
        public UserRepository(IConfiguration configuration) : base(configuration) { }

        public void create(User obj)
        {
            Execute(false, "INSERT INTO users(login, password) VALUES(@1, @2)", obj.Login, obj.Password);
        }

        public void delete(long id)
        {
            Execute(false, "DELETE FROM users WHERE id = @1", id);
        }

        public User FindByLogin(string login)
        {
            List<Dictionary<String, Object>> fields;
            fields = Execute(true, "SELECT id, login, password FROM users WHERE login = @1", login);
            if (fields.Count != 1) return null;
            return new User
            {
                Id = Convert.ToInt32(fields[0]["id"]),
                Login = fields[0]["login"].ToString(),
                Password = fields[0]["password"].ToString()
            };
        }

        public User FindByUser(User user)
        {
            List<Dictionary<String, Object>> fields;
            fields = Execute(true, "SELECT id, login, password FROM users WHERE login = @1 AND password = @2", user.Login, user.Password);
            if (fields.Count != 1) return null;
            user.Id = Convert.ToInt32(fields[0]["id"]);
            return user;
        }

        public User read(long id)
        {
            List<Dictionary<String, Object>> fields;
            fields = Execute(true, "SELECT login, password FROM users WHERE id = @1", id);
            if (fields.Count != 1) return null;
            return new User
            {
                Id = id,
                Login = fields[0]["login"].ToString(),
                Password = fields[0]["password"].ToString()
            };
        }

        public void update(User obj)
        {
            Execute(false, "UPDATE users SET login = @2, password = @3 WHERE id = @1", obj.Id, obj.Login, obj.Password);
        }
    }
}
