﻿using System;
using System.Collections.Generic;
using WebApplication1.Models;
using WebApplication1.Repository.Dao;

namespace WebApplication1.Service
{
    public class AnketService
    {
        private readonly IAnketDao anketDao;
        private readonly IQuestionDao questionDao;
        private readonly IAnswerDao answerDao;

        public AnketService(IAnketDao anketDao, IQuestionDao questionDao, IAnswerDao answerDao)
        {
            this.anketDao = anketDao;
            this.questionDao = questionDao;
            this.answerDao = answerDao;
        }

        public List<Anket> FindAll()
        {
            List<Anket> ankets = anketDao.FindAll();
            foreach (Anket anket in ankets)
            {
                List<Question> questions = questionDao.FindByAnket(anket);
                foreach (Question question in questions)
                {
                    question.Answers = answerDao.FindByQuestion(question);
                }
                anket.Questions = questions;
            }
            return ankets;
        }

        public void Save(Anket anket)
        {
            if (anket.Id == 0)
            {
                anketDao.create(anket);
            }
            else
            {
                anketDao.update(anket);
            }
        }

        public Anket FindById(long id)
        {
            Anket anket = anketDao.read(id);
            List<Question> questions = questionDao.FindByAnket(anket);
            foreach (Question question in questions)
            {
                question.Answers = answerDao.FindByQuestion(question);
            }
            anket.Questions = questions;
            return anket;
        }

        public void Remove(long id)
        {
            anketDao.delete(id);
        }

        public Boolean CheckName(String name)
        {
            return anketDao.FindByName(name) == null ? true : false;
        }
    }
}
