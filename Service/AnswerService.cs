﻿using System.Collections.Generic;
using WebApplication1.Models;
using WebApplication1.Repository.Dao;

namespace WebApplication1.Service
{
    public class AnswerService
    {
        private readonly IQuestionDao questionDao;
        private readonly IAnswerDao answerDao;

        public AnswerService(IQuestionDao questionDao, IAnswerDao answerDao)
        {
            this.questionDao = questionDao;
            this.answerDao = answerDao;
        }

        public void Save(Answer answer)
        {
            if (answer.Id == 0)
            {
                answerDao.create(answer);
            }
            else
            {
                answerDao.update(answer);
            }
        }

        public void Remove(long id)
        {
            answerDao.delete(id);
        }

        public Answer FindById(long id)
        {
            Answer answer = answerDao.read(id);
            answer.Question = questionDao.read(answer.Question_id);
            return answer;
        }

        public List<Answer> FindByQuestionId(long id)
        {
            Question question = questionDao.read(id);
            if (question == null) return null;
            return question.Answers = answerDao.FindByQuestion(question);
        }

        public int CountAnswersByQuestionId(long id)
        {
            return answerDao.CountAnswersByQuestionId(id);
        }
        public int CountTrueAnswersByQuestionId(long id)
        {
            return answerDao.CountTrueAnswersByQuestionId(id);
        }

        public int CountUniqueText(string text, long id)
        {
            return answerDao.CountAnswerWithTextAndQuestionId(text, id);
        }

    }
}
