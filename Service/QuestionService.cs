﻿using System;
using System.Collections.Generic;
using WebApplication1.Models;
using WebApplication1.Repository.Dao;

namespace WebApplication1.Service
{
    public class QuestionService
    {
        private readonly IAnketDao anketDao;
        private readonly IQuestionDao questionDao;
        private readonly IAnswerDao answerDao;

        public QuestionService(IAnketDao anketDao, IQuestionDao questionDao, IAnswerDao answerDao)
        {
            this.anketDao = anketDao;
            this.questionDao = questionDao;
            this.answerDao = answerDao;
        }

        public List<Question> FindByAnketId(long id)
        {
            Anket anket = anketDao.read(id);
            if (anket == null) return null;

            List<Question> questions = questionDao.FindByAnket(anket);
            foreach (Question question in questions)
            {
                question.Answers = answerDao.FindByQuestion(question);
            }
            anket.Questions = questions;
            return questions;
        }

        public Question FindById(long id)
        {
            Question question = questionDao.read(id);
            question.Anket = anketDao.read(question.Anket_id);
            return question;
        }

        public void Save(Question question)
        {
            if(question.Id == 0)
            {
                questionDao.create(question);
            }
            else
            {
                questionDao.update(question);
            }
        }

        public void Remove(long id)
        {
            questionDao.delete(id);
        }

        public Boolean CheckName(String name)
        {
            return questionDao.FindByName(name) == null ? true : false;
        }

    }
}
