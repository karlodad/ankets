﻿using System;
using System.Collections.Generic;
using System.Linq;
using WebApplication1.Models;
using WebApplication1.Repository.Dao;

namespace WebApplication1.Service
{
    public class ResultService
    {
        private readonly IAnketDao anketDao;
        private readonly IQuestionDao questionDao;
        private readonly IAnswerDao answerDao;
        private readonly IUserDao userDao;
        private readonly IResultDao resultDao;

        public ResultService(IAnketDao anketDao, IQuestionDao questionDao, IAnswerDao answerDao, IUserDao userDao, IResultDao resultDao)
        {
            this.anketDao = anketDao;
            this.userDao = userDao;
            this.resultDao = resultDao;
            this.questionDao = questionDao;
            this.answerDao = answerDao;
        }

        public List<Result> FindByLogin(String login)
        {
            User user = userDao.FindByLogin(login);
            List<Result> results = resultDao.FindByUser(user);
            foreach(Result result in results)
            {
                result.Anket = anketDao.read(result.Anket_id);
            }
            return results;
        }

        public Result FindById(long id)
        {
            Result result = resultDao.read(id);
            result.User = userDao.read(result.User_id);
            result.Anket = anketDao.read(result.Anket_id);

            List<Question> questions = questionDao.FindByAnketAndQuestionsId(result.Anket, result.Questions);
            foreach (Question question in questions)
            {
                question.Answers = answerDao.FindByQuestion(question);
                foreach(Answer answer in question.Answers)
                {
                    if (result.Answers.Contains((int) answer.Id))
                    {
                        answer.Checks = true;
                    }
                    else
                    {
                        answer.Checks = false;
                    }
                }
            }
            result.Anket.Questions = questions;
            return result;
        }


        public Result Save(Dictionary<String, String> values, String login, int anket_id, DateTime db, DateTime de)
        {
            Dictionary<int, int> optional_values = values.Keys
                .Where(key => Int32.TryParse(key, out int n))
                .ToDictionary(key => Convert.ToInt32(key), key => values[key].Equals("null") ? -1 : Convert.ToInt32(values[key]));
            List<int> list_quest = optional_values.Keys.ToList();
            List<int> list_ans = optional_values.Values.ToList();

            User user = userDao.FindByLogin(login);
            Anket anket = anketDao.read(anket_id);
            List<Question> questions = questionDao.FindByAnketAndQuestionsId(anket, list_quest.ToArray());
            foreach (Question question in questions)
            {
                question.Answers = answerDao.FindByQuestion(question);
            }
            anket.Questions = questions;

            List<int> list_true_ans = questions
                .Where(q => q.Answers.Where(a => a.Checks).ToList().Count != 0)
                .Select(q => q.Answers.Where(a => a.Checks).Select(a => a.Id).First())
                .Select(a => Convert.ToInt32(a))
                .ToList();
            int count_true_ans = list_true_ans.Count;
            int count_user_ans = list_true_ans
                .Where(e => list_ans.Contains(e))
                .Select(e => e)
                .ToList()
                .Count;                        
            Result result = new Result()
            {
                Anket_id = anket.Id,
                Anket = anket,
                User_id = user.Id,
                User = user,
                Date_begin = db,
                Date_end = de,
                Score = count_true_ans == 0 ? 100 : (count_user_ans * 100) / count_true_ans,
                Second = (int) (de - db).TotalSeconds,
                Answers = list_ans.ToArray(),
                Questions = list_quest.ToArray()
            };

            resultDao.create(result);
            return result;
        }

        public List<Statistic> FindAllStatistic()
        {
            List<Statistic> list = new List<Statistic>();
            Statistic stat = resultDao.GetMaxAvgScore();
            if (stat != null) list.Add(stat);

            stat = resultDao.GetMinAvgScore();
            if (stat != null) list.Add(stat);

            stat = resultDao.GetMostPopularTest();
            if (stat != null) list.Add(stat);

            stat = resultDao.GetMaxSecondTest();
            if (stat != null) list.Add(stat);
            return list;
        }
    }
}
