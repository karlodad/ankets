﻿using System;
using System.Collections.Generic;
using WebApplication1.Models;
using WebApplication1.Repository.Dao;

namespace WebApplication1.Service
{
    public class UserService
    {
        private readonly IUserDao userDao;

        public UserService(IUserDao userDao)
        {
            this.userDao = userDao;
        }

        public User CheckUser(User user)
        {
            return userDao.FindByUser(user);
        }

        public User CheckLogin(User user)
        {
            return userDao.FindByLogin(user.Login);
        }

        public User Register(User user)
        {
            userDao.create(user);
            return userDao.FindByLogin(user.Login);
        }

        public User FindByName(String name)
        {
            return userDao.FindByLogin(name);
        }
    }
}
