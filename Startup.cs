using Microsoft.AspNetCore.Authentication.Cookies;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using WebApplication1.Repository;
using WebApplication1.Repository.Dao;
using WebApplication1.Service;

namespace WebApplication1
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddAuthentication(CookieAuthenticationDefaults.AuthenticationScheme)
            .AddCookie(options =>
            {
                options.LoginPath = new Microsoft.AspNetCore.Http.PathString("/Account/Login");
                options.AccessDeniedPath = new Microsoft.AspNetCore.Http.PathString("/Home/Index");
            });

            services.AddControllersWithViews();
            services.AddSingleton<IConfiguration>(Configuration);

            services.AddTransient<IUserDao, UserRepository>();
            services.AddTransient<IAnketDao, AnketRepository>();
            services.AddTransient<IQuestionDao, QuestionRepository>();
            services.AddTransient<IAnswerDao, AnswerRepository>();
            services.AddTransient<IResultDao, ResultRepository>();

            services.AddScoped<UserService>();
            services.AddScoped<ResultService>();
            services.AddScoped<AnswerService>();
            services.AddScoped<QuestionService>();
            services.AddScoped<AnketService>();
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            app.UseStaticFiles();
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                app.UseExceptionHandler("/Home/Error");
            }

            app.UseRouting();

            app.UseAuthentication();
            app.UseAuthorization();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllerRoute(
                    name: "default",
                    pattern: "{controller=Home}/{action=Index}/{id?}");

                endpoints.MapControllerRoute(
                    name: "Form2Lvl",
                    pattern: "{controller}/{action}");

            });
        }
    }
}
