﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace WebApplication1.ViewModels
{
    public class LoginModel
    {
        [Required(ErrorMessage = "Не указан Login")]
        [StringLength(20, MinimumLength = 4, ErrorMessage = "Длина строки должна быть от 4 до 20 символов")]
        public string Login { get; set; }

        [Required(ErrorMessage = "Не указан пароль")]
        [StringLength(40, MinimumLength = 4, ErrorMessage = "Длина строки должна быть от 4 до 40 символов")]
        [DataType(DataType.Password)]
        public string Password { get; set; }

        public Boolean Admin { get; set; }
    }
}
