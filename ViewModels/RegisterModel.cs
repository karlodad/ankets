﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace WebApplication1.ViewModels
{
    public class RegisterModel
    {
        [Required(ErrorMessage = "Не указан Login")]
        [StringLength(20, MinimumLength = 4, ErrorMessage = "Длина строки должна быть от 4 до 20 символов")]
        [Remote(action: "CheckLogin", controller: "Account", ErrorMessage = "Login уже используется")]
        public string Login { get; set; }

        [Required(ErrorMessage = "Не указан пароль")]
        [StringLength(40, MinimumLength = 4, ErrorMessage = "Длина строки должна быть от 4 до 40 символов")]
        [DataType(DataType.Password)]
        public string Password { get; set; }

        [StringLength(40, MinimumLength = 4, ErrorMessage = "Длина строки должна быть от 4 до 40 символов")]
        [DataType(DataType.Password)]
        [Compare("Password", ErrorMessage = "Пароли не совпадают")]
        public string ConfirmPassword { get; set; }
    }
}
