﻿CREATE DATABASE ankets;
CREATE SEQUENCE tables_id_seq;

create table users(
	id integer NOT NULL DEFAULT nextval('tables_id_seq') PRIMARY KEY,
	login varchar(20) NOT NULL UNIQUE,
	password varchar(40) NOT NULL
);

create table ankets(
	id integer NOT NULL DEFAULT nextval('tables_id_seq') PRIMARY KEY,
	name varchar(20) NOT NULL UNIQUE
);

create table questions(
	id integer NOT NULL DEFAULT nextval('tables_id_seq') PRIMARY KEY,
	name varchar(100) NOT NULL,
	anket_id integer NOT NULL REFERENCES ankets(id) ON DELETE CASCADE,
	UNIQUE(name, anket_id)
);

create table answers(
	id integer NOT NULL DEFAULT nextval('tables_id_seq') PRIMARY KEY,
	checks boolean NOT NULL,
	text varchar(50) NOT NULL,
	question_id integer NOT NULL REFERENCES questions(id) ON DELETE CASCADE,
	UNIQUE(text, question_id)
);

create table results(
	id integer NOT NULL DEFAULT nextval('tables_id_seq') PRIMARY KEY,
	score integer NOT NULL,
	date_begin timestamp NOT NULL,
	date_end timestamp NOT NULL,
	second integer NOT NULL,
	ans integer[] NOT NULL,
	quest integer[] NOT NULL,
	user_id integer NOT NULL REFERENCES users(id) ON DELETE CASCADE,
	anket_id integer NOT NULL REFERENCES ankets(id) ON DELETE CASCADE
);
